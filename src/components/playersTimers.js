import React, { Component } from "react";
import '../css/playerTimers.css';
import { Link } from "react-router-dom";


class PlayerTimers extends Component{
    constructor(props) {
        super(props);
        this.state = {fieldClassName: this.props.name};
    }
    render(){
    return(
    <div className="timersDisplay">
    <div className="timeTitle">Time to play</div>
    <div className="clock">{("0" + this.props.minutes).slice(-2)} : {("0" + this.props.seconds).slice(-2)}</div>
    <Link to="/">{this.props.gameOver}</Link>
    </div>
    )
    }
}


export default PlayerTimers;