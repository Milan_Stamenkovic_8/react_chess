import React, { Component } from "react";
import '../css/promotionPieces.css';
import PromotionPiece from "./promotionPiece";


class PromotionPieces extends Component{
    setPromotionPieces = () => {
        if(this.props.promoClass === "promoDisplayWhite")
            return "white";
        else
            if(this.props.promoClass === "promoDisplayBlack")
                return "black";
            else
                return "default";
    }
    promotionClasses = () =>{
        let promotionColor = this.setPromotionPieces();
        if(promotionColor === "white")
            return ["promoPieceWhiteQueen", "promoPieceWhiteRook", "promoPieceWhiteBishop", "promoPieceWhiteKnight"];
        else{
            if(promotionColor === "black")
                return ["promoPieceBlackQueen", "promoPieceBlackRook", "promoPieceBlackBishop", "promoPieceBlackKnight"];
            else
                return ["promoPieceDefault", "promoPieceDefault", "promoPieceDefault", "promoPieceDefault"];
        }
    }
    childCallback = (childData) => {
        this.sendDataToParent(childData);
    }
    sendDataToParent = (data) => {
        this.props.parentCallback(data);
     }
    render(){
    return(
    <div className={this.props.promoClass}>
    <PromotionPiece parentCallback={this.childCallback} promoClass={this.promotionClasses()[0]}></PromotionPiece>
    <PromotionPiece parentCallback={this.childCallback} promoClass={this.promotionClasses()[1]}></PromotionPiece>
    <PromotionPiece parentCallback={this.childCallback} promoClass={this.promotionClasses()[2]}></PromotionPiece>
    <PromotionPiece parentCallback={this.childCallback} promoClass={this.promotionClasses()[3]}></PromotionPiece>
    </div>
    )
    }   
}


export default PromotionPieces;