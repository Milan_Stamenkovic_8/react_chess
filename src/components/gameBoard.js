import React, { Component } from "react";
import '../css/gameBoard.css';
import BoardField from "./boardField";
import { indexRow, indexColumn, whitePawn, blackPawn } from "../models/InitialData";
import { validMove } from "../validation/movesValidation";
import { kingSafetyCheck, safePositionAroundKing } from "../validation/checkValidation";
import { generateBoard, generateBoardAnpasan } from "../supportFunctions/BoardGenerator";


class GameBoard extends Component{
    constructor(props) {
        super(props);
        this.state = {selectedField: "", selectedPiece: "", toPlay: this.props.turn,
                      boardChanged: false, anPasan: [-1, -1, 0], boardState: this.props.boardState.slice()};
    }
    createFields = () => {
        let fields = [];
        let fieldClass;
        let row = 0;

        for(let i=0; i<8; i++){
            for(let j=0; j<8; j++){
                if((i+j)%2 === 0){
                    fieldClass = "white";
                }
                else{
                    fieldClass = "black";
                }
            fields.push(<BoardField name={fieldClass} key={`${i}${j}`} id={i+j+row}
                                    piece={this.state.boardState[i][j]}></BoardField>)
            }
        row = row + 7;
        }
        return fields;
    }
    selectField = (event) => {
        let fields = document.getElementsByClassName("gameBoard")[0].children;
        let currentField = event.target.id;
        let currentPiece = event.target.getAttribute("value");
        let row = 0;
        for(let i=0; i<8; i++){
            for(let j=0; j<8; j++){
                if((i+j+row).toString() === currentField && fields[currentField].getAttribute("value") !== ""
                && fields[currentField].getAttribute("value").substring(0,5) === this.props.turn){
                    this.setState({selectedField: currentField, selectedPiece: currentPiece, boardChanged: false});
                    fields[currentField].className = fields[currentField].className + " light";
                }
                else{
                    fields[i+j+row].className = fields[i+j+row].className.substring(0,5);
                    this.setState({boardChanged: false});
                }
            }
            row = row + 7;
        } 
    }
    sendDataToParent = (data) => {
        setTimeout(()=>{
            if(this.state.boardChanged)
                this.props.parentCallback(data)
            else 
                this.props.parentCallback("don't change")
        })
    }
    changeBoardState = (event, boardState) => {
        let moveTo = event.target.id;
        let selected = this.state.selectedField;
        setTimeout(() => {
           if(selected !== "" && selected !== moveTo){
               let src = boardState[indexRow[selected]][indexColumn[selected]];
               let dst = boardState[indexRow[moveTo]][indexColumn[moveTo]];
               if(src.substring(0,5) !== dst.substring(0,5)){
               boardState[indexRow[selected]][indexColumn[selected]] = ""
               boardState[indexRow[moveTo]][indexColumn[moveTo]] = src;
               this.setState({selectedField: "", selectedPiece: "", boardChanged: true,
                              anPasan: [this.state.anPasan[0], this.state.anPasan[1], this.state.anPasan[2]-1]});
               }
           }
        });
    }

    copyBoard = (boardState) => {
        let newBoard = [];
        for(let i = 0; i<8; i++){
            newBoard[i] = [];
            for(let j = 0; j<8; j++){
                newBoard[i][j] = boardState[i][j];
            }
        }
        return newBoard;
    }

    anPasanPossibility = (src, dst, board) => {
        setTimeout(() => {
            if(src !== dst){
                if((indexRow[src] === 6 && indexRow[dst] === 4 && 
                    board[indexRow[src]][indexColumn[src]] === whitePawn.type)
                || (indexRow[src] === 1 && indexRow[dst] === 3 && 
                    board[indexRow[src]][indexColumn[src]] === blackPawn.type))
                    this.setState({anPasan: [indexRow[dst], indexColumn[src], 2]});
            }   
        });        
    }

    changeBoardStateAnPasan = (event, boardState) => {
        let moveTo = event.target.id;
        let selected = this.state.selectedField;
        setTimeout(() => {
           if(selected !== "" && selected !== moveTo){
               let src = boardState[indexRow[selected]][indexColumn[selected]];
               let dst = boardState[indexRow[moveTo]][indexColumn[moveTo]];
               if(src.substring(0,5) !== dst.substring(0,5)){
               boardState[indexRow[selected]][indexColumn[selected]] = ""
               boardState[indexRow[moveTo]][indexColumn[moveTo]] = src;
               boardState[indexRow[selected]][indexColumn[moveTo]] = "";
               this.setState({selectedField: "", selectedPiece: "", anPasan: [-1, -1, 0], boardChanged: true});
               }
           }
        });
    }

    checkMate = () => {//fali funkcija koja proverava da li neka figura prekida stanje saha
        if(kingSafetyCheck(this.state.selectedPiece.substring(0,5), this.state.boardState) === false){
            if(safePositionAroundKing(this.state.boardState, this.state.selectedPiece.substring(0,5)) === "attacked")
                return true;
            else 
                return false;
        }
        else
            return false;
    }

    changeBoardStateCastling = (event, boardState) => {
        let moveTo = event.target.id;
        let selected = this.state.selectedField;
        setTimeout(() => {
           if(selected !== "" && selected !== moveTo){
               let src = boardState[indexRow[selected]][indexColumn[selected]];
               let dst = boardState[indexRow[moveTo]][indexColumn[moveTo]];
               if(src.substring(0,5) !== dst.substring(0,5)){
               boardState[indexRow[selected]][indexColumn[selected]] = ""
               boardState[indexRow[moveTo]][indexColumn[moveTo]] = src;
               if(indexColumn[selected] > indexColumn[moveTo]){
                    boardState[indexRow[selected]][indexColumn[moveTo]+1] = boardState[indexRow[selected]][indexColumn[selected]-4];
                    boardState[indexRow[selected]][indexColumn[selected]-4] = "";
               }
               else{
                boardState[indexRow[selected]][indexColumn[moveTo]-1] = boardState[indexRow[selected]][indexColumn[selected]+3];
                boardState[indexRow[selected]][indexColumn[selected]+3] = "";
               }
               this.setState({selectedField: "", selectedPiece: "", anPasan: [-1, -1, 0], boardChanged: true});
               }
           }
        });
    }

    validateMove = (event) => {
        let src = this.state.selectedField;
        let dst = event.target.id;
        let validate;
        this.anPasanPossibility(src, dst, this.state.boardState);
        if(src !== "" && src !== dst)
            validate = validMove([indexRow[src],indexColumn[src]], [indexRow[dst], indexColumn[dst]], 
                                 this.state.anPasan, this.state.boardState);
        return validate;
    }

    positionSafe = (event) => {
        const oldBoard = this.copyBoard(this.state.boardState);
        let moveTo = event.target.id;
        let selected = this.state.selectedField;
        let src = [indexRow[selected], indexColumn[selected]];
        let dst = [indexRow[moveTo], indexColumn[moveTo]];
        let newBoard;
        let pieceMoveValidation = this.validateMove(event);
        if(pieceMoveValidation === true){
            newBoard = generateBoard(src, dst, oldBoard);
        }
        else{
            if(pieceMoveValidation === "anpasan")
                newBoard = generateBoardAnpasan(src, dst, oldBoard);
        }
        let safeKing = kingSafetyCheck(this.state.selectedPiece.substring(0,5), newBoard);
        return safeKing; 
    }

    promotePawn = (board) => {
        if(this.props.promotion !== undefined && this.props.promote !== ""){
            if(this.props.promotion.substring(0, 5) === "White"){
                for(let i=0; i<8; i++){
                    if(board[0][i] === whitePawn.type)
                        board[0][i] = this.props.promotion;
                }
            }
            else{
                if(this.props.promotion.substring(0, 5) === "Black"){
                    for(let i=0; i<8; i++){
                        if(board[7][i] === blackPawn.type)
                            board[7][i] = this.props.promotion;
                    }
                }
            }
        }
    }
    getClassName = () => {
        if(this.props.disable === "disable")
            return "gameBoardDisabled"
        else
            return "gameBoard"
    }
    render(){
        this.promotePawn(this.state.boardState);
        return(
            <div className={this.getClassName()} onClick={(event)=> {
                this.selectField(event);
                let pieceMoveValidation = this.validateMove(event);
                
                if(pieceMoveValidation === true){
                   if(this.positionSafe(event))
                        this.changeBoardState(event, this.state.boardState);
                }
            
                if(pieceMoveValidation === "anpasan"){
                    if(this.positionSafe(event))
                        this.changeBoardStateAnPasan(event, this.state.boardState);
                }
                
                if(pieceMoveValidation === "castling"){
                    this.changeBoardStateCastling(event, this.state.boardState);    
                }
                if(this.checkMate() === false){
                    this.sendDataToParent("changePlayer");
                }
                
            }}>
                {this.createFields()}
            </div>
        )
    }
}


export default GameBoard;