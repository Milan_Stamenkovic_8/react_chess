import React, { Component } from "react";
import '../css/gamePage.css';
import GameBoard from "./gameBoard";
import { initialState } from "../models/InitialData";
import PlayerTimers from "./playersTimers";
import PromotionPieces from "./promotionPieces";
import { pawnPromotionStatus } from "../validation/movesValidation";


class GamePage extends Component{
    constructor(props) {
        super(props);
        this.state = {boardState: initialState, toPlay: "White", moveFrom: "", pieceToMove: "", 
                      promoPiece: "", promoteTo: "Default", intervalID: setInterval(()=>{},1000),
                      minutes: 2, seconds: 59};
    }
    changePlayer = () => {
        this.state.toPlay === "White" ? this.setState({toPlay: "Black"}) : this.setState({toPlay: "White"});
        this.setState({minutes: 2, seconds: 59});
        clearInterval(this.state.intervalID);
    }
    setPlayerTimer = () => {
        this.setState({intervalID: setInterval(() => {
            this.setState({seconds: this.state.seconds - 1});
        }, 1000)})
    }
    setArrowWhite = () => {
        if(this.state.toPlay === "White")
            return "playerWArrow"
        else
            return "playerArrowInvisible"
    }
    setArrowBlack = () => {
        if(this.state.toPlay === "Black")
            return "playerBArrow"
        else
            return "playerArrowInvisible"
    }
    setPromoClass = () => {
        let promoPiece = pawnPromotionStatus(this.state.boardState);
        if(promoPiece === "whitePromotion")
            return "promoDisplayWhite";
        else{
            if(promoPiece === "blackPromotion")
                return "promoDisplayBlack";
            else
                return "promoDisplay";
        }
    }
    setGameOverData = () => {
        if(this.state.minutes === 0 && this.state.seconds === 0){
            if(this.state.toPlay === "White")
                return "GAME OVER. WINNER: PLAYER 2";
            else
                return "GAME OVER. WINNER: PLAYER 1";
        }
        else
            return "";
    }
    disableBoard = () => {
        if(this.state.minutes === 0 && this.state.seconds === 0){
            if(this.state.toPlay === "White")
                return "disable";
        }
    }
    childCallbackEndTurn = (childData) => {
        if(childData === "changePlayer"){
            this.changePlayer();
            this.setPlayerTimer();
        }
    }
    childCallbackPromotion = (childData) => {
        this.setState({promoteTo: childData.substring(10)});
    }
    componentWillUnmount() {
        clearInterval(this.state.intervalID);
    }
    componentDidMount(){
        this.setPlayerTimer()
    }
    componentDidUpdate(){
        if(this.state.seconds === 0 && this.state.minutes === 0){
            clearInterval(this.state.intervalID);
        }
        else{
            if(this.state.seconds === 0 && this.state.minutes > 0){
            this.setState({minutes: this.state.minutes - 1, seconds: 59});
        }}
    }
    render(){
        return(
            <div className="gamePage">
            <div className="playerOneLabel">Player 2:&nbsp; <span className="playerB">Black</span>
            <span id="barrow" className={this.setArrowBlack()}>&nbsp; &larr; to play</span>
            </div>
            <GameBoard parentCallback={this.childCallbackEndTurn} turn={this.state.toPlay} 
                       boardState={this.state.boardState} promotion={this.state.promoteTo}
                       disable={this.disableBoard()}>
            </GameBoard>
            <div className="playerTwoLabel">Player 1:&nbsp; <span className="playerW">White</span>
            <span id="warrow" className={this.setArrowWhite()}>&nbsp; &larr; to play</span>
            </div>
            <PromotionPieces parentCallback={this.childCallbackPromotion} promoClass={this.setPromoClass()}>
            </PromotionPieces>
            <PlayerTimers minutes={this.state.minutes} seconds={this.state.seconds} gameOver={this.setGameOverData()}>
            </PlayerTimers>
            <nav className="fixed-bottom">
            <div className="bottom">&copy; Milan</div>
            </nav>  
            </div>
        )
    }
}


export default GamePage;