import React, { Component } from "react";
import '../css/boardField.css';


class BoardField extends Component{
    constructor(props) {
        super(props);
        this.state = {fieldClassName: this.props.name};
    }
    render(){
    return(
    <div className={this.state.fieldClassName} id={this.props.id} value={this.props.piece}>
    <div id={this.props.id} className={this.props.piece} value={this.props.piece}></div>
    </div>
    )
    }
}


export default BoardField;