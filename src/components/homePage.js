import React, { Component } from "react";
import { Link } from "react-router-dom";
import '../css/homePage.css';

class HomePage extends Component{
    render(){
        return(
            <>
            <div className="homePage">
            <h1>Welcome to <label className="gamename">Chessmaster</label></h1>
            <Link to="/game" className="btn btn-dark btnstart">Start Game</Link>
            <nav className="fixed-bottom">
            <div className="bottom">&copy; Milan</div>
            </nav>  
            </div>
            </>
        )
    }
}

export default HomePage;