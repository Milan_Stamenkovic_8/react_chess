import React, { Component } from "react";
import '../css/promotionPiece.css';


class PromotionPiece extends Component{
    sendDataToParent = (data) => {
       this.props.parentCallback(data);
    }
    render(){
    return(
    <div className={this.props.promoClass} onClick={(event) => {
        this.sendDataToParent(event.target.className);
    }}></div>
    )
    }
}


export default PromotionPiece;