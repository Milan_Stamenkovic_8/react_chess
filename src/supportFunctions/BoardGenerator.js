export function generateBoard(src, dst, oldBoard){
    let board = oldBoard;
    if(src !== [] && dst !== [] && src !== dst && src !== undefined && dst !== undefined){
        if(board[src[0]][src[1]].substring(0,5) !== board[dst[0]][dst[1]].substring(0,5)){
            board[dst[0]][dst[1]] = board[src[0]][src[1]];
            board[src[0]][src[1]] = "";
        }
    }
    return board;
}

export function generateBoardAnpasan(src, dst, oldBoard){
    let board = oldBoard;
    if(src !== [] && dst !== [] && src !== dst && src !== undefined && dst !== undefined){
        if(board[src[0]][src[1]].substring(0,5) !== board[dst[0]][dst[1]].substring(0,5)){
            board[dst[0]][dst[1]] = board[src[0]][src[1]];
            board[src[0]][dst[1]] = "";
            board[src[0]][src[1]] = "";
        }
    }
    return board;
}