import { whitePawn, blackPawn, whiteRook, blackRook, whiteKnight, blackKnight, whiteBishop, blackBishop, whiteQueen, blackQueen, whiteKing, blackKing } from "../models/InitialData";
import { kingSafetyCheck, getPositionState } from "./checkValidation";

export function validMove(src, dst, anPasan, board){
    if(src !== "" && src !== undefined && dst !== "" && dst !== undefined && board !== undefined){
        if(dst[0] !== undefined && dst[1] !== undefined){
            if(board[dst[0]][dst[1]].substring(5,9) !== whiteKing.type.substring(5,9))
                switch(board[src[0]][src[1]]){
                    case whitePawn.type:
                        return whitePawnValidMove(src, dst, anPasan, board);
                    case blackPawn.type:
                        return blackPawnValidMove(src, dst, anPasan, board);
                    case whiteRook.type:
                    case blackRook.type:
                        return rookValidMove(src, dst, board);
                    case whiteKnight.type:
                    case blackKnight.type:
                        return knightValidMove(src, dst);
                    case whiteBishop.type:
                    case blackBishop.type:
                        return bishopValidMove(src, dst, board);
                    case whiteQueen.type:
                    case blackQueen.type:
                        return queenValidMove(src, dst, board);
                    case whiteKing.type:
                    case blackKing.type:
                        return kingValidMove(src, dst, board);
                    default:
                        return true;
                }
            else
                return false;
        }
        else
            return false;
    }
    else
        return false;
}


function whitePawnValidMove(src, dst, anPasan, board){
    if(src[0] > dst[0] && (src[0]-dst[0]) < 3){
        if(src[1] === dst[1]){
            return whitePawnMovementVertical(src, dst, board);
        }
        else{
            if(anPasan[2] > 0 && board[dst[0]][dst[1]] === "")
                return whitePawnAnPasan(src, dst, board);
            else
                return whitePawnMovementDiagonal(src, dst, board);
        }
    }
    else
        return false;
}

function whitePawnMovementVertical(src, dst, board){
    if((src[0]-dst[0]) === 1  && board[dst[0]][dst[1]] === ""){
        return true;
    }
    else{
        if(src[0] === 6 && board[src[0]-1][src[1]] === "" && board[dst[0]][dst[1]] === "")
            return true;
        else 
            return false;
    }
}

function whitePawnMovementDiagonal(src, dst, board){
    if((src[1]-dst[1]) === 1 || (dst[1]-src[1]) === 1){
        if((src[0]-dst[0]) === 1 && board[dst[0]][dst[1]] !== "")
            return true;
        else
            return false;
    }
    else
        return false; 
}

function whitePawnAnPasan(src, dst, board){
    if((src[1]-dst[1]) === 1 || (dst[1]-src[1]) === 1){
        if((src[0]-dst[0]) === 1 && board[dst[0]][dst[1]] === ""){
            if(src[1] > dst[1]){
                if(board[src[0]][dst[1]] === blackPawn.type)
                    return "anpasan"
                else
                    return false;
            }
            else{
                if(board[src[0]][dst[1]] === blackPawn.type)
                    return "anpasan";                
                else
                    return false;
            }     
        }
        else
            return false;
    }
    else
        return false; 
}

function blackPawnValidMove(src, dst, anPasan, board){
    if(src[0] < dst[0] && (dst[0]-src[0]) < 3){
        if(src[1] === dst[1]){
            return blackPawnMovementVertical(src, dst, board);
        }
        else{
            if(anPasan[2] > 0 && board[dst[0]][dst[1]] === "")
                return blackPawnAnPasan(src, dst, board);
            else
                return blackPawnMovementDiagonal(src, dst, board);
        }
    }
    else
        return false;
}

function blackPawnMovementVertical(src, dst, board){
    if((dst[0]-src[0]) === 1  && board[dst[0]][dst[1]] === ""){
        return true;
    }
    else{
        if(src[0] === 1 && board[src[0]+1][src[1]] === "" && board[dst[0]][dst[1]] === "")
            return true;
        else 
            return false;
    }
}

function blackPawnMovementDiagonal(src, dst, board){
    if((src[1]-dst[1]) === 1 || (dst[1]-src[1]) === 1){
        if((dst[0]-src[0]) === 1 && board[dst[0]][dst[1]] !== "")
            return true;
        else
            return false;
    }
    else
        return false; 
}

function blackPawnAnPasan(src, dst, board){
    if((src[1]-dst[1]) === 1 || (dst[1]-src[1]) === 1){
        if((dst[0]-src[0]) === 1 && board[dst[0]][dst[1]] === ""){
            if(src[1] > dst[1]){
                if(board[src[0]][dst[1]] === whitePawn.type)
                    return "anpasan"
                else
                    return false;
            }
            else{
                if(board[src[0]][dst[1]] === whitePawn.type)
                    return "anpasan";                
                else
                    return false;
            }     
        }
        else
            return false;
    }
    else
        return false; 
}

function rookValidMove(src, dst, board){
    if(src[0] === dst[0] || src[1] === dst[1]){
        if(src[0] === dst[0]){
            return pieceMovementHorizontal(src, dst, board);
        }
        else{
            return pieceMovementVertical(src, dst, board);
        }
    }
    else
        return false;
}


function knightValidMove(src, dst){
    if(src[0] > dst[0]){
        return knightMovementUpward(src, dst);
    }
    else{
        if(src[0] < dst[0]){
            return knightMovementDownward(src, dst);
        }
        else
            return false;
    }
}

function knightMovementUpward(src, dst){
    if((src[0]-dst[0]) < 3){
        if((src[0]-dst[0]) === 1){
            return knightMovementOneVerticalTwoHorizontal(src, dst);
        }
        else{
            return knightMovementTwoVerticalOneHorizontal(src, dst);
        }
    }
    else
        return false;
}

function knightMovementDownward(src, dst){
    if((dst[0]-src[0]) < 3){
        if((dst[0]-src[0]) === 1){
           return knightMovementOneVerticalTwoHorizontal(src, dst);
        }
        else{
            return knightMovementTwoVerticalOneHorizontal(src, dst);
        }
    }
    else
        return false;
}

function knightMovementOneVerticalTwoHorizontal(src, dst){
    if(src[1] > dst[1]){
        if((src[1]-dst[1]) === 2)
            return true;
        else 
            return false;
    }
    else{
        if(src[1] < dst[1]){
            if((dst[1]-src[1]) === 2)
                return true;
            else
                return false;
        }
        else
            return false;
    }
}

function knightMovementTwoVerticalOneHorizontal(src, dst){
    if(src[1] > dst[1]){
        if((src[1]-dst[1]) === 1)
            return true;
        else 
            return false;
    }
    else{
        if(src[1] < dst[1]){
            if((dst[1]-src[1]) === 1)
                return true;
            else
                return false;
        }
        else
            return false;
    }
}


function bishopValidMove(src, dst, board){
    if(src[0] < dst[0]){
        return pieceMovementDiagonalDownward(src, dst, board);
    }
    else{
        if(src[0] > dst[0]){
            return pieceMovementDiagonalUpward(src, dst, board);
        }
        else
            return false;
    }
}


function queenValidMove(src, dst, board){
    if(src[0] === dst[0] || src[1] === dst[1]){
        return queenMovementHorizontalOrVertical(src, dst, board);
    }
    else{
        return queenMovementDiagonal(src, dst, board);
    }
}

function queenMovementHorizontalOrVertical(src, dst, board){
    if(src[0] === dst[0]){
        return pieceMovementHorizontal(src, dst, board);
    }
    else{
        return pieceMovementVertical(src, dst, board);
    }
}

function queenMovementDiagonal(src, dst, board){
    if(src[0] < dst[0]){
        return pieceMovementDiagonalDownward(src, dst, board);
    }
    else{
        if(src[0] > dst[0]){
            return pieceMovementDiagonalUpward(src, dst, board);
        }
        else
            return false;
    }
}

function kingValidMove(src, dst, board){
    if((src[0] === dst[0] && (src[1]-dst[1]) === 1) || src[1] === dst[1]){
        return kingMovementHorizontalOrVertical(src, dst);
    }
    else{
        if((src[0]-dst[0] === 1 && src[1]-dst[1] === 1) || (src[0]-dst[0] === 1 && dst[1]-src[1] === 1)
        || (dst[0]-src[0] === 1 && src[1]-dst[1] === 1) || (dst[0]-src[0] === 1 && dst[1]-src[1] === 1))
            return kingMovementDiagonal(src, dst);
        else{
            if(src[0] === 7 && src[1] === 4 && board[src[0]][src[1]] === whiteKing.type){
                return kingMovementCastling(src, dst, board, whiteKing.type);
            }
            else{
                if(src[0] === 0 && src[1] === 4 && board[src[0]][src[1]] === blackKing.type){
                    return kingMovementCastling(src, dst, board, blackKing.type);
                }
                else
                    return false;
            }
        }
    }
}

function kingMovementCastling(src, dst, board, king){
    if(king === whiteKing.type){
        if(dst[0] === 7 && dst[1] === 2){
            return kingMovementCastlingLeft(src, dst, board, whiteRook.type)
        }
        else{
            if(dst[0] === 7 && dst[1] === 6)
                return kingMovementCastlingRight(src, dst, board, whiteRook.type)
            else 
                return false;
        }
    }
    else{
        if(dst[0] === 0 && dst[1] === 2){
            return kingMovementCastlingLeft(src, dst, board, blackRook.type)
        }
        else{
            if(dst[0] === 0 && dst[1] === 6)
                return kingMovementCastlingRight(src, dst, board, blackRook.type)
            else
                return false;
        }
    }
}

function kingMovementCastlingLeft(src, dst, board, rook){
    if(board[src[0]][src[1]-4] === rook){
        if(kingSafetyCheck(rook.substring(0,5), board) === true){
            if(pieceMovementHorizontal(src, dst, board) === true){
                if(getPositionState([src[0],[src[1]-1]], board, rook.substring(0,5)) === "guarded"){
                    if(getPositionState([src[0],[src[1]-2]], board, rook.substring(0,5)) === "guarded")
                        return "castling";
                }
                else
                    return false;
            }
            else 
                return false;
        }
        else
            return false;
    }
    else
        return false;
}

function kingMovementCastlingRight(src, dst, board, rook){
    if(board[src[0]][src[1]+3] === rook){
        if(kingSafetyCheck(rook.substring(0,5), board) === true){
            if(pieceMovementHorizontal(src, dst, board) === true){
                if(getPositionState([src[0],[src[1]+1]], board, rook.substring(0,5)) === "guarded"){
                    if(getPositionState([src[0],[src[1]+2]], board, rook.substring(0,5)) === "guarded")
                        return "castling";
                }
                else
                    return false;
            }
            else 
                return false;
        }
        else
            return false;
    }
    else
        return false;
}

function kingMovementHorizontalOrVertical(src, dst){
    if(src[0] === dst[0]){
        return kingMovementHorizontal(src, dst);
    }
    else{
        return kingMovementVertical(src, dst);
    }
}

function kingMovementHorizontal(src, dst){
    if(src[1] < dst[1])
        return ((dst[1]-src[1]) === 1);
    else
        return ((src[1]-dst[1]) === 1);
}

function kingMovementVertical(src, dst){
    if(src[0] < dst[0])
        return ((dst[0]-src[0]) === 1);
    else
        return ((src[0]-dst[0]) === 1);
}

function kingMovementDiagonal(src, dst){
    if(src[0] < dst[0]){
        return kingMovementDiagonalDownward(src, dst);
    }
    else{
        if(src[0] > dst[0]){
            return kingMovementDiagonalUpward(src, dst);
        }
        else
            return false;
    }
}

function kingMovementDiagonalDownward(src, dst){
    if(src[1] < dst[1]){
        if((dst[0] - src[0]) === (dst[1]-src[1]))
            return ((dst[0]-src[0]) === 1);
        else
            return false;
    }
    else{
        if(src[1] > dst[1]){
            if((dst[0] - src[0]) === (src[1]-dst[1]))
                return ((dst[0]-src[0]) === 1);
            else
                return false;
        }
        else
            return false;
    }
}

function kingMovementDiagonalUpward(src, dst){
    if(src[1] < dst[1]){
        if((src[0] - dst[0]) === (dst[1]-src[1]))
            return ((src[0]-dst[0]) === 1);
        else
            return false;
    }
    else{
        if(src[1] > dst[1]){
            if((src[0] - dst[0]) === (src[1]-dst[1]))
                return ((src[0]-dst[0]) === 1);
            else
                return false;
        }
        else
            return false;
    }
}

function pieceMovementHorizontal(src, dst, board){
    if(src[1] < dst[1])
        return checkEmptyPathHorizontal(src[0], (src[1]+1), (dst[1]-1), board);
    else
        return checkEmptyPathHorizontal(src[0], (dst[1]+1), (src[1]-1), board);
}

function pieceMovementVertical(src, dst, board){
    if(src[0] < dst[0])
        return checkEmptyPathVertical(src[1], (src[0]+1), (dst[0]-1), board);
    else
        return checkEmptyPathVertical(src[1], (dst[0]+1), (src[0]-1), board);
}

function pieceMovementDiagonalUpward(src, dst, board){
    if(src[1] < dst[1]){
        if((src[0] - dst[0]) === (dst[1]-src[1]))
            return checkEmptyPathAntiDiagonal((dst[0]+1), (src[0]-1), (dst[1]-1), (src[1]+1), board);
        else
            return false;
    }
    else{
        if(src[1] > dst[1]){
            if((src[0] - dst[0]) === (src[1]-dst[1]))
                return checkEmptyPathMainDiagonal((dst[0]+1), (src[0]-1), (dst[1]+1), (src[1]-1), board);
            else
                return false;
        }
        else
            return false;
    }
}

function pieceMovementDiagonalDownward(src, dst, board){
    if(src[1] < dst[1]){
        if((dst[0] - src[0]) === (dst[1]-src[1]))
            return checkEmptyPathMainDiagonal((src[0]+1), (dst[0]-1), (src[1]+1), (dst[1]-1), board);
        else
            return false;
    }
    else{
        if(src[1] > dst[1]){
            if((dst[0] - src[0]) === (src[1]-dst[1]))
                return checkEmptyPathAntiDiagonal((src[0]+1), (dst[0]-1), (src[1]-1), (dst[1]+1), board);
            else
                return false;
        }
        else
            return false;
    }
}

function checkEmptyPathHorizontal(rowIndex, start, destination, board){
    for(let j=start; j<=destination; j++){
        if(board[rowIndex][j] !== "")
            return false;
        }
    return true;
}

function checkEmptyPathVertical(columnIndex, start, destination, board){
    for(let i=start; i<=destination; i++){
        if(board[i][columnIndex] !== "")
            return false;
        }
    return true;
}

function checkEmptyPathMainDiagonal(srcRow, dstRow, srcColumn, dstColumn, board){
    let i = srcRow;
    let j = srcColumn;
    while(i <= dstRow && j <= dstColumn){
        if(board[i][j] !== ""){
            return false;
        }
        i++;
        j++;
    }
    return true;
}

function checkEmptyPathAntiDiagonal(srcRow, dstRow, srcColumn, dstColumn, board){
    let i = srcRow;
    let j = srcColumn;
    while(i <= dstRow && j >= dstColumn){
        if(board[i][j] !== ""){
            return false;
        }
        i++;
        j--;
    }
    return true;
}

export function pawnPromotionStatus(board){
    for(let i=0; i<8; i++){
        if(board[0][i] === whitePawn.type)
            return "whitePromotion";
        else{
            if(board[7][i] === blackPawn.type)
                return "blackPromotion";
        }
    }
    return "noPromotion";
}