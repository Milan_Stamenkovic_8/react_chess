import { whiteKing, blackRook, blackQueen, whiteRook, whiteQueen, blackPawn, blackBishop, whitePawn, whiteBishop, blackKing, blackKnight, whiteKnight } from "../models/InitialData";

export function kingSafetyCheck(playerTurn, board){
    let kingPosition = getKingPosition(playerTurn, board);
    let kingState = getPositionState(kingPosition, board, playerTurn);
    if(kingState === "guarded")
        return true;
    else
        return false;
}

export function safePositionAroundKing(board, playerTurn){
    let kingPosition = getKingPosition(playerTurn, board);
    let emptyPositionsAroundKing = getEmptyPositionsAroundKing(kingPosition, board);
    for(let i=0; i<emptyPositionsAroundKing.length; i++){
        if(getPositionState(emptyPositionsAroundKing[i], board, playerTurn) === "guarded")
            return "guarded";
    }
    return "attacked";
}

export function getPositionState(position, board, playerTurn){
    if(position === "" || position === undefined){
        return false;
    }
    else{
        if(playerTurn === "White"){
            return searchForAttackers(position, board, "Black");
        }
        else{
            if(playerTurn === "Black"){
                return searchForAttackers(position, board, "White");
            }
        }
    }
}

function searchForAttackers(position, board, attackers){
    if(position[0] === 0){
        return searchForAttackersDownFirstLine(position, board, attackers);
    }
    else{
        if(position[0] === 7){
            return searchForAttackersUpLastLine(position, board, attackers);
        }
        else{
            if(position[1] === 0)
                return searchForAttackersRightFirstColumn(position, board, attackers);
            else{
                if(position[1] === 7)
                    return searchForAttackersLeftLastColumn(position, board, attackers);
                else
                    return searchForAttackersInAllDirections(position, board, attackers);
            }
        }
    }
}

function searchForAttackersDownFirstLine(position, board, attackers){
    if(position[1] === 0)
        return findAttackersDownRight(position, board, attackers);
    else{
        if(position[1] === 7)
            return findAttackersDownLeft(position, board, attackers);
        else
            return findAttackersDown(position, board, attackers);
    }
}

function searchForAttackersUpLastLine(position, board, attackers){
    if(position[1] === 0)
        return findAttackersUpRight(position, board, attackers);
    else{
        if(position[1] === 7)
            return findAttackersUpLeft(position, board, attackers);
        else
            return findAttackersUp(position, board, attackers);
    }
}

function searchForAttackersRightFirstColumn(position, board, attackers){
    if(findAttackersUpRight(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findAttackersDownRight(position, board, attackers) === "attacked")
            return "attacked";
        else
            return "guarded";
    }
}

function searchForAttackersLeftLastColumn(position, board, attackers){
    if(findAttackersUpLeft(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findAttackersDownLeft(position, board, attackers) === "attacked")
            return "attacked";
        else
            return "guarded";
    }
}

function searchForAttackersInAllDirections(position, board, attackers){
    if(findAttackersDown(position, board, attackers) === "attacked")
        return "attacked"
    else{
        if(findAttackersUp(position, board, attackers) === "attacked")
            return "attacked"
        else 
            return "guarded";
    }
}

function findAttackersDownRight(position, board, attackers){
    if(findRightHorizontalAttacker(position, board, attackers) === "attacked")
        return "attacked"
    else{
        if(findDownVerticalAttacker(position, board, attackers) === "attacked")
            return "attacked"
        else{
            if(findRightMainDiagonalAttacker(position, board, attackers) === "attacked")
                return "attacked";
            else{
                if(findDownRightKnightPositionAttacker(position, board, attackers) === "attacked")
                    return "attacked";
                else 
                    return "guarded"
            }
        }
    }
}

function findAttackersDownLeft(position, board, attackers){
    if(findLeftHorizontalAttacker(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findDownVerticalAttacker(position, board, attackers) === "attacked")
            return "attacked"
        else{
            if(findLeftAntiDiagonalAttacker(position, board, attackers) === "attacked")
                return "attacked";
            else{
                if(findDownLeftKnightPositionAttacker(position, board, attackers) === "attacked")
                    return "attacked";
                else 
                    return "guarded"
            }
        }
    }
}

function findAttackersDown(position, board, attackers){
    if(findAttackersDownLeft(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findAttackersDownRight(position, board, attackers) === "attacked")
            return "attacked";
        else 
            return "guarded";
    }
}

function findAttackersUpRight(position, board, attackers){
    if(findRightHorizontalAttacker(position, board, attackers) === "attacked")
        return "attacked"
    else{
        if(findUpVerticalAttacker(position, board, attackers) === "attacked")
            return "attacked"
        else{
            if(findRightAntiDiagonalAttacker(position, board, attackers) === "attacked")
                return "attacked";
            else{
                if(findUpRightKnightPositionAttacker(position, board, attackers) === "attacked")
                    return "attacked";
                else 
                    return "guarded"
            }
        }
    }
}

function findAttackersUpLeft(position, board, attackers){
    if(findLeftHorizontalAttacker(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findUpVerticalAttacker(position, board, attackers) === "attacked")
            return "attacked";
        else{
            if(findLeftMainDiagonalAttacker(position, board, attackers) === "attacked")
                return "attacked";
            else{
                if(findUpLeftKnightPositionAttacker(position, board, attackers) === "attacked")
                    return "attacked";
                else 
                    return "guarded";
            }
        }
    }
}

function findAttackersUp(position, board, attackers){
    if(findAttackersUpLeft(position, board, attackers) === "attacked")
        return "attacked";
    else{
        if(findAttackersUpRight(position, board, attackers) === "attacked")
            return "attacked";
        else 
            return "guarded";
    }
}

function findLeftHorizontalAttacker(position, board, attackers){
    if(position[1]-2 > 0){
        if(board[position[0]][position[1]-2] === blackKing.type && attackers === "Black")
            return "attacked";
        else{
            if(board[position[0]][position[1]-2] === whiteKing.type && attackers === "White")
                return "attacked";
        }
    }
    for(let j = (position[1]-1); j>=0; j--){
        if(attackers === "Black"){
            if(board[position[0]][j] === blackRook.type || board[position[0]][j] === blackQueen.type)
                return "attacked";
            else{
                if(board[position[0]][j].substring(0,5) === "White")
                    return "guarded";
                else{
                    if(board[position[0]][j] === blackPawn.type 
                    || board[position[0]][j] === blackKnight.type
                    || board[position[0]][j] === blackBishop.type
                    || board[position[0]][j] === blackKing.type)
                        return "guarded";
                }
            }
        }
        else{
            if(board[position[0]][j] === whiteRook.type 
            || board[position[0]][j] === whiteQueen.type)
                return "attacked";
            else{
                if(board[position[0]][j].substring(0,5) === "Black")
                    return "guarded";
                else{
                    if(board[position[0]][j] === whitePawn.type 
                    || board[position[0]][j] === whiteKnight.type
                    || board[position[0]][j] === whiteBishop.type
                    || board[position[0]][j] === whiteKing.type)
                        return "guarded";
                }
            }
        }
    }
    return "guarded";
}

function findLeftMainDiagonalAttacker(position, board, attackers){
    let i = position[0] - 1;
    let j = position[1] - 1;
    let attacked = "";
    while(i >= 0 && j >= 0){
        if(attackers === "Black"){
            if((position[0]-i) === 1 && (position[1]-j) === 1){
                attacked = findQueenOrBishopOrPawnAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        else{
            if((position[0]-i) === 1 && (position[1]-j) === 1){
                attacked = findQueenOrBishopOrPawnAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        --i;
        --j;
    }
    return "guarded";
}

function findLeftAntiDiagonalAttacker(position, board, attackers){
    let i = position[0] + 1;
    let j = position[1] - 1;
    let attacked = "";
    while(i <= 7 && j >= 0){
        if(attackers === "Black"){
            if((i-position[0]) === 1 && (position[1]-j) === 1){
                attacked = findQueenOrBishopOrPawnAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        else{
            if((i-position[0]) === 1 && (position[1]-j) === 1){
                attacked = findQueenOrBishopOrPawnAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        i++;
        j--;
    }
    return "guarded";
}

function findUpVerticalAttacker(position, board, attackers){
    if(position[0]-2 > 0){
        if(board[position[0]-2][position[1]] === blackKing.type && attackers === "Black")
            return "attacked";
        else{
            if(board[position[0]-2][position[1]] === whiteKing.type && attackers === "White")
                return "attacked";
        }
    }
    for(let i=(position[0]-1); i>=0; i--){
        if(attackers === "Black"){
            if(board[i][position[1]] === blackRook.type || board[i][position[1]] === blackQueen.type)
                return "attacked";
            else{
                if(board[i][position[1]].substring(0,5) === "White")
                    return "guarded";
                else{
                    if(board[i][position[1]] === blackKnight.type || board[i][position[1]] === blackBishop.type
                    || board[i][position[1]] === blackPawn.type || board[i][position[1]] === blackKing.type)
                        return "guarded";
                }
            }
        }
        else{
            if(board[i][position[1]] === whiteRook.type || board[i][position[1]] === whiteQueen.type)
                return "attacked";
            else{
                if(board[i][position[1]].substring(0,5) === "Black")
                    return "guarded";
                else{
                    if(board[i][position[1]] === whiteKnight.type || board[i][position[1]] === whiteBishop.type
                    || board[i][position[1]] === blackPawn.type || board[i][position[1]] === whiteKing.type)
                        return "guarded";
                }
            }
        }
    }
    return "guarded";
}

function findDownVerticalAttacker(position, board, attackers){
    if(position[0]+2 < 7){
        if(board[position[0]+2][position[1]] === blackKing.type && attackers === "Black")
            return "attacked";
        else{
            if(board[position[0]+2][position[1]] === whiteKing.type && attackers === "White")
                return "attacked";
        }
    }
    for(let i=(position[0]+1); i<=7; i++){
        if(attackers === "Black"){
            if(board[i][position[1]] === blackRook.type || board[i][position[1]] === blackQueen.type)
                return "attacked";
            else{
                if(board[i][position[1]].substring(0,5) === "White")
                    return "guarded";
                else{
                    if(board[i][position[1]] === blackKnight.type || board[i][position[1]] === blackBishop.type
                    || board[i][position[1]] === blackPawn.type || board[i][position[1]] === blackKing.type)
                        return "guarded";
                }
            }
        }
        else{
            if(board[i][position[1]] === whiteRook.type || board[i][position[1]] === whiteQueen.type)
                return "attacked";
            else{
                if(board[i][position[1]].substring(0,5) === "Black")
                    return "guarded";
                else{
                    if(board[i][position[1]] === whiteKnight.type || board[i][position[1]] === whiteBishop.type
                    || board[i][position[1]] === whitePawn.type || board[i][position[1]] === whiteKing.type)
                        return "guarded";
                }
            }
        }
    }
    return "guarded";
}

function findRightHorizontalAttacker(position, board, attackers){
    if(position[1]+2 < 7){
        if(board[position[0]][position[1]+2] === blackKing.type && attackers === "Black")
            return "attacked";
        else{
            if(board[position[0]][position[1]+2] === whiteKing.type && attackers === "White")
                return "attacked";
        }
    }
    for(let j = (position[1]+1); j<=7; j++){
        if(attackers === "Black"){
            if(board[position[0]][j] === blackRook.type || board[position[0]][j] === blackQueen.type)
                return "attacked";
            else{
                if(board[position[0]][j].substring(0,5) === "White")
                    return "guarded";
                else{
                    if(board[position[0]][j] === blackKnight.type || board[position[0]][j] === blackBishop.type
                    || board[position[0]][j] === blackPawn.type || board[position[0]][j] === blackKing.type)
                        return "guarded";
                }
            }
        }
        else{
            if(board[position[0]][j] === whiteRook.type || board[position[0]][j] === whiteQueen.type)
                return "attacked";
            else{
                if(board[position[0]][j].substring(0,5) === "Black")
                    return "guarded";
                else{
                    if(board[position[0]][j] === whiteKnight.type || board[position[0]][j] === whiteBishop.type
                    || board[position[0]][j] === whitePawn.type || board[position[0]][j] === whiteKing.type)
                        return "guarded";
                }
            }
        }
    }
    return "guarded";
}

function findRightMainDiagonalAttacker(position, board, attackers){
    let i = position[0] + 1;
    let j = position[1] + 1;
    let attacked = "";
    while(i <= 7 && j <= 7){
        if(attackers === "Black"){
            if((i-position[0]) === 1 && (j-position[1]) === 1){
                attacked = findQueenOrBishopOrPawnAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        else{
            if((i-position[0]) === 1 && (j-position[1]) === 1){
                attacked = findQueenOrBishopOrPawnAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        i++;
        j++;
    }
    return "guarded";
}

function findRightAntiDiagonalAttacker(position, board, attackers){
    let i = position[0] - 1;
    let j = position[1] + 1;
    let attacked = "";
    while(i >= 0 && j <= 7){
        if(attackers === "Black"){
            if((position[0]-i) === 1 && (j-position[1]) === 1){
                attacked = findQueenOrBishopOrPawnAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerBlack(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        else{
            if((position[0]-i) === 1 && (j-position[1]) === 1){
                attacked = findQueenOrBishopOrPawnAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
            else{
                attacked = findQueenOrBishopAttackerWhite(i, j, board);
                if(attacked !== "")
                    return attacked;
            }
        }
        i--;
        j++;
    }
    return "guarded";
}

function findQueenOrBishopOrPawnAttackerBlack(i, j, board){
    if(board[i][j] === blackPawn.type || board[i][j] === blackQueen.type
    || board[i][j] === blackBishop.type || board[i][j] === blackKing.type)
        return "attacked";
    else{
        if(board[i][j].substring(0,5) === "White")
            return "guarded";
        else{
            if(board[i][j] === blackKnight.type || board[i][j] === blackRook.type)
                return "guarded";
            else
                return "";
        }
    }
}

function findQueenOrBishopAttackerBlack(i, j, board){
    if(board[i][j] === blackQueen.type || board[i][j] === blackBishop.type)
        return "attacked";
    else{
        if(board[i][j].substring(0,5) === "White")
            return "guarded";
        else{
            if(board[i][j] === blackPawn.type || board[i][j] === blackKnight.type
            || board[i][j] === blackRook.type || board[i][j] === blackKing.type)
                return "guarded";
            else
                return "";
        }
    }
}

function findQueenOrBishopOrPawnAttackerWhite(i, j, board){
    if(board[i][j] === whitePawn.type || board[i][j] === whiteQueen.type
    || board[i][j] === whiteBishop.type || board[i][j] === whiteKing.type)
        return "attacked";
    else{
        if(board[i][j].substring(0,5) === "Black")
            return "guarded";
        else{
            if(board[i][j] === whiteRook.type || board[i][j] === whiteKnight.type)
                return "guarded";
            else
                return "";
        }
    }
}

function findQueenOrBishopAttackerWhite(i, j, board){
    if(board[i][j] === whiteQueen.type || board[i][j] === whiteBishop.type)
        return "attacked";
    else{
        if(board[i][j].substring(0,5) === "Black")
            return "guarded";
        else{
            if(board[i][j] === whitePawn.type || board[i][j] === whiteRook.type 
            || board[i][j] === whiteKnight.type || board[i][j] === whiteKing.type)
                return "guarded";
            else
                return "";
        }
    }
}

function findDownRightKnightPositionAttacker(position, board, attackers){
    if(attackers === "Black"){
        if((position[0]+2) < 7){
            return findDownOneOrTwoRightOneOrTwoKnightPositionAttackerBlack(position, board);
        }
        else{
            return findDownOneRightTwoKnightPositionAttackerBlack(position, board);
        }
    }
    else{
        if((position[0]+2) < 7){
            return findDownOneOrTwoRightOneOrTwoKnightPositionAttackerWhite(position, board);
        }
        else{
            return findDownOneRightTwoKnightPositionAttackerWhite(position, board);
        }
    }
}

function findDownOneOrTwoRightOneOrTwoKnightPositionAttackerBlack(position, board){
    if((position[1]+2) < 7){
        if(board[position[0]+1][position[1]+2] === blackKnight.type
        || board[position[0]+2][position[1]+1] === blackKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]+1) < 7){
            if(board[position[0]+2][position[1]+1] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findDownOneRightTwoKnightPositionAttackerBlack(position, board){
    if((position[0]+1) < 7){
        if((position[1]+2) < 7){
            if(board[position[0]+1][position[1]+2] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findDownOneOrTwoRightOneOrTwoKnightPositionAttackerWhite(position, board){
    if((position[1]+2) < 7){
        if(board[position[0]+1][position[1]+2] === whiteKnight.type
        || board[position[0]+2][position[1]+1] === whiteKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]+1) < 7){
            if(board[position[0]+2][position[1]+1] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findDownOneRightTwoKnightPositionAttackerWhite(position, board){
    if((position[0]+1) < 7){
        if((position[1]+2) < 7){
            if(board[position[0]+1][position[1]+2] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findDownLeftKnightPositionAttacker(position, board, attackers){
    if(attackers === "Black"){
        if((position[0]+2) < 7){
            return findDownOneOrTwoLeftOneOrTwoKnightPositionAttackerBlack(position, board);
        }
        else{
            return findDownOneLeftTwoKnightPositionAttackerBlack(position, board);
        }
    }
    else{
        if((position[0]+2) < 7){
           return findDownOneOrTwoLeftOneOrTwoKnightPositionAttackerWhite(position, board);
        }
        else{
            return findDownOneLeftTwoKnightPositionAttackerWhite(position, board);
        }
    }
}

function findDownOneOrTwoLeftOneOrTwoKnightPositionAttackerBlack(position, board){
    if((position[1]-2) > 0){
        if(board[position[0]+1][position[1]-2] === blackKnight.type
        || board[position[0]+2][position[1]-1] === blackKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]-1) > 0){
            if(board[position[0]+2][position[1]-1] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findDownOneLeftTwoKnightPositionAttackerBlack(position, board){
    if((position[0]+1) < 7){
        if((position[1]-2) > 0){
            if(board[position[0]+1][position[1]-2] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findDownOneOrTwoLeftOneOrTwoKnightPositionAttackerWhite(position, board){
    if((position[1]-2) > 0){
        if(board[position[0]+1][position[1]-2] === whiteKnight.type
        || board[position[0]+2][position[1]-1] === whiteKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]-1) > 0){
            if(board[position[0]+2][position[1]-1] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findDownOneLeftTwoKnightPositionAttackerWhite(position, board){
    if((position[0]+1) < 7){
        if((position[1]-2) > 0){
            if(board[position[0]+1][position[1]-2] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findUpRightKnightPositionAttacker(position, board, attackers){
    if(attackers === "Black"){
        if((position[0]-2) > 0){
            return findUpOneOrTwoRightOneOrTwoKnightPositionAttackerBlack(position, board);
        }
        else{
            return findUpOneRightTwoKnightPositionAttackerBlack(position, board);
        }
    }
    else{
        if((position[0]-2) > 0){
            return findUpOneOrTwoRightOneOrTwoKnightPositionAttackerWhite(position, board);
        }
        else{
            return findUpOneRightTwoKnightPositionAttackerWhite(position, board);
        }
    }
}

function findUpOneOrTwoRightOneOrTwoKnightPositionAttackerBlack(position, board){
    if((position[1]+2) < 7){
        if(board[position[0]-1][position[1]+2] === blackKnight.type
        || board[position[0]-2][position[1]+1] === blackKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]+1) < 7){
            if(board[position[0]-2][position[1]+1] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findUpOneRightTwoKnightPositionAttackerBlack(position, board){
    if((position[0]-1) > 0){
        if((position[1]+2) < 7){
            if(board[position[0]-1][position[1]+2] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findUpOneOrTwoRightOneOrTwoKnightPositionAttackerWhite(position, board){
    if((position[1]+2) < 7){
        if(board[position[0]-1][position[1]+2] === whiteKnight.type
        || board[position[0]-2][position[1]+1] === whiteKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]+1) < 7){
            if(board[position[0]-2][position[1]+1] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findUpOneRightTwoKnightPositionAttackerWhite(position, board){
    if((position[0]-1) > 0){
        if((position[1]+2) < 7){
            if(board[position[0]-1][position[1]+2] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findUpLeftKnightPositionAttacker(position, board, attackers){
    if(attackers === "Black"){
        if((position[0]-2) > 0){
            return findUpOneOrTwoLeftOneOrTwoKnightPositionAttackerBlack(position, board);
        }
        else{
            return findUpOneLeftTwoKnightPositionAttackerBlack(position, board);
        }
    }
    else{
        if((position[0]-2) > 0){
            return findUpOneOrTwoLeftOneOrTwoKnightPositionAttackerWhite(position, board);
        }
        else{
            return findUpOneLeftTwoKnightPositionAttackerWhite(position, board);
        }
    } 
}

function findUpOneOrTwoLeftOneOrTwoKnightPositionAttackerBlack(position, board){
    if((position[1]-2) > 0){
        if(board[position[0]-1][position[1]-2] === blackKnight.type
        || board[position[0]-2][position[1]-1] === blackKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]-1) > 0){
            if(board[position[0]-2][position[1]-1] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}


function findUpOneLeftTwoKnightPositionAttackerBlack(position, board){
    if((position[0]-1) > 0){
        if((position[1]-2) > 0){
            if(board[position[0]-1][position[1]-2] === blackKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function findUpOneOrTwoLeftOneOrTwoKnightPositionAttackerWhite(position, board){
    if((position[1]-2) > 0){
        if(board[position[0]-1][position[1]-2] === whiteKnight.type
        || board[position[0]-2][position[1]-1] === whiteKnight.type)
            return "attacked";
        else
            return "guarded";
    }
    else{
        if((position[1]-1) > 0){
            if(board[position[0]-2][position[1]-1] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
}

function findUpOneLeftTwoKnightPositionAttackerWhite(position, board){
    if((position[0]-1) > 0){
        if((position[1]-2) > 0){
            if(board[position[0]-1][position[1]-2] === whiteKnight.type)
                return "attacked";
            else
                return "guarded";
        }
        else
            return "guarded";
    }
    else
        return "guarded";
}

function getKingPosition(playerTurn, board){
    let kingPosition = [];
    for(let i=0; i<8; i++){
        for(let j=0; j<8; j++){
            if(playerTurn === "White"){
                if(board[i][j] === whiteKing.type)
                    kingPosition = [i, j];
            }
            else{
                if(playerTurn === "Black"){
                    if(board[i][j] === blackKing.type)
                        kingPosition = [i, j];
                }
            }
        }
    }
    return kingPosition;
}

function getEmptyPositionsAroundKing(kingPosition, board){
    let positions = [];
    let i = kingPosition[0];
    let j = kingPosition[1];
    if(i-1 > 0 && j-1 > 0){
    if(board[i-1][j-1] === ""){
        positions.push([i-1, j-1]);
    }
    }
    if(i-1 > 0){
    if(board[i-1][j] === ""){
        positions.push([i-1, j]);
    }
    }
    if(i-1 > 0 && j+1 < 7){
    if(board[i-1][j+1] === ""){
        positions.push([i-1, j+1]);
    }
    }
    if(j-1 > 0){
        if(board[i][j-1] === ""){
            positions.push([i, j-1]);
        }
    }
    if(j+1 < 7){
    if(board[i][j+1] === ""){
        positions.push([i, j+1]);
    }
    }
    if(i+1 < 7 && j-1 > 0){
    if(board[i+1][j-1] === ""){
        positions.push([i+1, j-1]);
    }
    }
    if(i+1 < 7){
    if(board[i+1][j] === ""){
        positions.push([i+1, j]);
    }
    }
    if(i+1 < 7 && j+1 <7){
    if(board[i+1][j+1] === ""){
        positions.push([i+1, j+1]);
    }
    }
    return positions;
}