export default class ChessPiece{
    constructor(id, type){
        this.id = id;
        this.type = type;
    }
}