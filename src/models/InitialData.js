import ChessPiece from "./ChessPiece";

export const emptyPiece = new ChessPiece(0, "");
export const blackRook = new ChessPiece(1, "BlackRook");
export const whiteRook = new ChessPiece(2, "WhiteRook");
export const blackKnight = new ChessPiece(3, "BlackKnight");
export const whiteKnight = new ChessPiece(4, "WhiteKnight");
export const blackBishop = new ChessPiece(5, "BlackBishop");
export const whiteBishop = new ChessPiece(6, "WhiteBishop");
export const blackQueen = new ChessPiece(7, "BlackQueen");
export const whiteQueen = new ChessPiece(8, "WhiteQueen");
export const blackKing = new ChessPiece(9, "BlackKing");
export const whiteKing = new ChessPiece(10, "WhiteKing");
export const blackPawn = new ChessPiece(11, "BlackPawn");
export const whitePawn = new ChessPiece(12, "WhitePawn");

export let initialState = [];

for(let i = 0; i < 8; i++){
    initialState[i] = []
    for(let j = 0; j < 8; j++){
        switch(`${i}${j}`){
            case "00":
            case "07":
                initialState[i][j] = blackRook.type;
                break;
            case "70":
            case "77":
                initialState[i][j] = whiteRook.type;
                break;
            case "01":
            case "06":
                initialState[i][j] = blackKnight.type;
                break;
            case "71":
            case "76":
                initialState[i][j] = whiteKnight.type;
                break;
            case "02":
            case "05":
                initialState[i][j] = blackBishop.type;
                break;
            case "72":
            case "75":
                initialState[i][j] = whiteBishop.type;
                break;
            case "03":
                initialState[i][j] = blackQueen.type;
                break;
            case "73":
                initialState[i][j] = whiteQueen.type;
                break;
            case "04":
                initialState[i][j] = blackKing.type;
                break;
            case "74":
                initialState[i][j] = whiteKing.type;
                break;
            case `1${j}`:
                initialState[i][j] = blackPawn.type;
                break;
            case `6${j}`:
                initialState[i][j] = whitePawn.type;
                break;
            default:
                initialState[i][j] = emptyPiece.type;
        }
    }
}

export let indexRow = [0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7];
export let indexColumn = [0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7,0,1,2,3,4,5,6,7];