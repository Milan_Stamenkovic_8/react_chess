import React from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePage from './components/homePage';
import GamePage from './components/gamePage';

function App() {
  return (
    <Router>
    <div className="App">
    <Route path = "/" exact component = { HomePage }/>
    <Route path = "/game" exact component = { GamePage }/>
    </div>
    </Router>
  );
}

export default App;
